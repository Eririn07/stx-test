package stx.service;

import stx.domain.Xml;

import java.util.List;

public interface ConsumeXMLService {
    List<Xml> getXml(Integer userId);
}
