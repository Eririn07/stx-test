package stx.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import stx.domain.Xml;
import stx.wrapper.XmlWrapper;

import java.util.List;

@Service
@Slf4j
public class ConsumeXMLServiceImpl implements ConsumeXMLService{

    RestTemplate rest = new RestTemplate();

    public List<Xml> getXml(Integer userId){
        XmlWrapper response = rest.getForObject("http://localhost:8888/user/"+userId, XmlWrapper.class);

        List<Xml> res = response.getWrapper();

        log.debug("test: {}", res);

        return res;
    }
}
