package stx.wrapper;

import jakarta.xml.bind.annotation.XmlList;
import lombok.Getter;
import lombok.Setter;
import stx.domain.Xml;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class XmlWrapper {

    private List<Xml> wrapper;

    public XmlWrapper(){
        this.wrapper = new ArrayList<>();
    }
}
