package stx.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stx.domain.Xml;
import stx.repository.XmlRepository;
import stx.service.ConsumeXMLService;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/")
public class DataController {

    private final XmlRepository xmlRepository;
    private final ConsumeXMLService xmlService;

    public DataController(XmlRepository xmlRepository, ConsumeXMLService xmlService) {
        this.xmlRepository = xmlRepository;
        this.xmlService = xmlService;
    }

    @GetMapping(value="/{id}", produces = {"application/xml"})
    public ResponseEntity<Xml> getOneData(@PathVariable Integer id){
        Xml res = xmlRepository.findOneById(id);
        log.debug("result: {}",res);
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<Xml> entityModel
                = new ResponseEntity<>(res, headers,
                HttpStatus.CREATED);

        return entityModel;
    }

//    @GetMapping(value="/user/{userId}", produces = {"application/xml"})
//    @JacksonXmlElementWrapper(useWrapping = false)
//    public ResponseEntity<List<Xml>> getDataByUser(@PathVariable Integer userId){
//        List<Xml> res = xmlRepository.findAllByUserId(userId);
//        HttpHeaders headers = new HttpHeaders();
//        ResponseEntity<List<Xml>> entityModel
//                = new ResponseEntity<>(res, headers,
//                HttpStatus.CREATED);
//
//        return entityModel;
//    }

    @GetMapping(value="/user/{userId}", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> getDataByUsers(@PathVariable Integer userId) throws JsonProcessingException {
        List<Xml> res = xmlRepository.findAllByUserId(userId);
        XmlMapper xmlMapper = new XmlMapper();
        String xml = xmlMapper.writer().withRootName("root").writeValueAsString(res);

        return ResponseEntity.status(HttpStatus.OK).body(xml);
    }


}
