package stx.repository;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.springframework.data.repository.CrudRepository;
import stx.domain.Xml;

import java.util.List;

public interface XmlRepository extends CrudRepository<Xml, Integer> {

    Xml findOneById(Integer id);

    List<Xml> findAllByUserId(Integer userId);
}
