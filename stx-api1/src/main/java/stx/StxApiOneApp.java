package stx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StxApiOneApp {
    public static void main(String[] args){
        SpringApplication.run(StxApiOneApp.class, args);
    }
}
