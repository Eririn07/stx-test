package stx.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.xml.bind.JAXBException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import stx.service.ConsumeXMLService;
import stx.service.UserService;
import stx.wrapper.Item;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/")
public class DataController {

    private final ConsumeXMLService xmlService;
    private final UserService userService;
    RestTemplate rest = new RestTemplate();

    public DataController(ConsumeXMLService xmlService, UserService userService) {
        this.xmlService = xmlService;
        this.userService = userService;
    }

    @GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Item>> getOneData(@PathVariable Integer id) throws JAXBException {
        List<Item> res = xmlService.getXml(id);

        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @GetMapping(value="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getOneUser(@PathVariable String id) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();

        String postXml = objectMapper.writeValueAsString(xmlService.getXml(Integer.parseInt(id)));

//        log.debug("Post: {}", postXml);

        String res = userService.getMainData(id);
        String res2 = userService.getAddress(id);
        String res3 = userService.getCompany(id);
        //simulate API3
        String response = rest.getForObject("https://jsonplaceholder.typicode.com/users/" + id+"/todos", String.class);
        log.debug("address: {}", res2);
        log.debug("company: {}", res3);
//        Optional<RedisData> res = userService.getDataFromRedis(id);
        JsonNode user = objectMapper.readTree(res);
        JsonNode address = objectMapper.readTree(res2);
        JsonNode company = objectMapper.readTree(res3);
        JsonNode post = objectMapper.readTree(postXml);
        JsonNode todos = objectMapper.readTree(response);

        ObjectNode mergedJson = objectMapper.createObjectNode();
        mergedJson.setAll((ObjectNode) user);
        mergedJson.set("address", address);
        mergedJson.set("company", company);
        mergedJson.set("posts", post);
        mergedJson.set("todos", todos);

        String jsonFinal = mergedJson.toString();

//        log.debug("final: {}", jsonFinal);

        if(res != null){
            return ResponseEntity.status(HttpStatus.OK).body(jsonFinal);
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(jsonFinal);
        }
    }
}
