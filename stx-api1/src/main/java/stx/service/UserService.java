package stx.service;

public interface UserService {
    String getMainData(String id);
    String getAddress(String hashKey);
    String getCompany(String hashKey);
}
