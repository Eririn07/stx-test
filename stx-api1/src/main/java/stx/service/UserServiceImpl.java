package stx.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final RedisTemplate<String, String> redisTemplate;

    public UserServiceImpl(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public String getMainData(String hashKey) {
        return getValueFromHash(hashKey, "main_data");
    }

    public String getAddress(String hashKey) {
        return getValueFromHash(hashKey, "address");
    }

    public String getCompany(String hashKey) {
        return getValueFromHash(hashKey, "company");
    }

    private String getValueFromHash(String hashKey, String fieldKey) {
        HashOperations<String, String, String> hashOps = redisTemplate.opsForHash();
        return hashOps.get(hashKey, fieldKey);
    }
}
