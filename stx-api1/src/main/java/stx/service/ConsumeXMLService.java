package stx.service;

import jakarta.xml.bind.JAXBException;
import stx.wrapper.Item;

import java.util.List;

public interface ConsumeXMLService {
    List<Item> getXml(Integer userId) throws JAXBException;
}
