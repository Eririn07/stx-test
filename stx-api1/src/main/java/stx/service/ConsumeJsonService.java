package stx.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import stx.wrapper.Todo;

import java.util.List;

public interface ConsumeJsonService {

    List<Todo> getJson(Integer userId) throws JsonProcessingException;
}
