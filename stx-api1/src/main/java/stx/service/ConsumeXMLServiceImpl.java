package stx.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import stx.wrapper.Item;
import stx.wrapper.Root;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ConsumeXMLServiceImpl implements ConsumeXMLService{

    RestTemplate rest = new RestTemplate();

    @Value("${data.location}")
    String IP;

    public List<Item> getXml(Integer userId) throws JAXBException {
        String response = rest.getForObject("http://"+IP+":8888/user/"+userId, String.class);

        JAXBContext context = JAXBContext.newInstance(Root.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        StringReader reader = new StringReader(response);
        Root root = (Root) unmarshaller.unmarshal(reader);

        List<Item> items = root.getItems();

        log.debug("res: {}", items.get(1).getTitle());

        return items;
    }
}
