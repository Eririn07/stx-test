package stx.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import stx.wrapper.Todo;

import java.util.List;

@Service
@Slf4j
public class ConsumeJsonServiceImpl implements ConsumeJsonService{

    RestTemplate rest = new RestTemplate();
     ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Link: https://jsonplaceholder.typicode.com/users/1/todos
     * */
    public List<Todo> getJson(Integer userId) throws JsonProcessingException {
        String response = rest.getForObject("https://jsonplaceholder.typicode.com/users/" + userId+"/todos", String.class);

        List<Todo> result = objectMapper.readValue(response, new TypeReference<List<Todo>>() {});

        return result;
    }
}
